var webpack = require('webpack');

module.exports = {
  devtool: 'source-map',
  entry: [
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './src/index'
  ],
  output: {
    path: __dirname + '/dist/',
    filename: 'index.js',
    publicPath: '/dist/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/, 
        loader: 'react-hot',
        exclude: /node_modules/
      },
      { 
        test: /\.jsx?$/, 
        loader: 'babel',
        query: {
          'plugins': ['babel-plugin-object-assign'],
          'stage': 0
        },
        exclude: /node_modules/
      }
    ]
  }
};
