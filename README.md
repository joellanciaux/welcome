Welcome!
=========================

### Steps to get up and running
Assuming you have git/node/npm installed, you'll need to do the following:

1. Clone the project `git clone https://bitbucket.org/joellanciaux/welcome.git`
1. `cd welcome`
1. `npm install`
1. `npm start`
1. Navigate to `http://localhost:3000`
1. Nice!

### Notes
This was put together partly for fun, using some things that I've been wanting to use but have been on my back-burner for a little bit. 

Most notably, I was wanting to dive a little deeper into [Redux](https://github.com/rackt/react-redux), [React Router 1.0 beta](https://github.com/rackt/react-router/tree/master), and [Radium](http://projects.formidablelabs.com/radium/).

####Enjoy!