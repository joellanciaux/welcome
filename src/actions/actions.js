import * as constants from '../constants/constants';
import fakeCredentialCheck from '../fake/fakeCredentialCheck';

export function login(username, password, router) {
  return dispatch => {

    // Indicate that we're starting a login.
    dispatch(submitLogin());

    // Fake API call.
    setTimeout(function() {
      if (username === fakeCredentialCheck.legitUsername && password === fakeCredentialCheck.legitPassword) {
        dispatch(loginSuccess(router))
      } else {
        dispatch(loginFailure())
      }
    }, 1000);
  };
}

export function submitLogin() {
  return {
    type: constants.LOGIN_SUBMIT
  };
}

export function loginSuccess(router) {
  return dispatch => {
    // Dispatch that we have a successful login.
    dispatch({
      type: constants.LOGIN_SUCCESS  
    });
    
    // Redirect to the 'welcome' page.
    router.transitionTo('/welcome')
  };
}

export function loginFailure() {
  return dispatch => {
    // Dispatch that we have a failed login.
    dispatch({
      type: constants.LOGIN_FAILURE  
    });

    // Clear error after 10 seconds.
    setTimeout(function() {
      dispatch(clearLoginFailureMessage());
    }, 10000);
  };
}

export function clearLoginFailureMessage() {
  return {
    type: constants.LOGIN_FAILURE_CLEAR
  };
}

export function logout(router) {
  return dispatch => {
    // Dispatch the logout
    dispatch({
      type: constants.LOGOUT  
    });
    
    // Redirect to the root.
    router.transitionTo('/')
  };
}