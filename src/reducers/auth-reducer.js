import * as constants from '../constants/constants';

const initialState = {
  isLoggingIn: false,
  isLoggedIn: false,
  hasFailedLogin: false
};

export function auth(state = initialState, action) {
  switch (action.type) {
  case constants.LOGIN_SUBMIT:
    return Object.assign({}, state, {
      hasFailedLogin: false,
      isLoggingIn: true
    });
  case constants.LOGIN_SUCCESS:
    return Object.assign({}, state, {
      hasFailedLogin: false,
      isLoggedIn: true,
      isLoggingIn: false
    });
  case constants.LOGIN_FAILURE:
    return Object.assign({}, state, {
      hasFailedLogin: true,
      isLoggingIn: false
    });
  case constants.LOGIN_FAILURE_CLEAR:
    return Object.assign({}, state, {
      hasFailedLogin: false
    });
  case constants.LOGOUT:
    return Object.assign({}, state, initialState);
  default:
    return state;
  }
}