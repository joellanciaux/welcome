import React from 'react';
import Radium from 'radium'
import { connect } from 'react-redux';
import { login, loginSuccess } from '../../actions/actions';
import fakeCredentialCheck from '../../fake/fakeCredentialCheck';

@Radium
class Login extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }

  componentDidMount() {
    let { isLoggedIn } = this.props;

    if (isLoggedIn) {
      this.props.dispatch(loginSuccess(this.context.router));
    }
  }

  inputChanged(propertyName, event) {
    let newState = {};
    newState[propertyName] = event.target.value;

    this.setState(newState);
  }

  handleSubmit(e) {
    e.preventDefault();

    // Attempt a login.
    this.props.dispatch(login(this.state.username, this.state.password, this.context.router));
  }

  render() {
    let { username, password } = this.state;
    let { isLoggingIn, isLoggedIn, hasFailedLogin } = this.props;
    let errorContent = null;
    let inputStyles = [styles.input];

    // If we have login failure, display the error message, error style
    if (hasFailedLogin) {
      let { legitUsername, legitPassword } = fakeCredentialCheck;
      errorContent = ( 
          <div style={styles.errorMessage}>Whoops. Please reenter your credentials (try {legitUsername}/{legitPassword}).</div>
      );
      inputStyles.push(styles.inputError)
    }

    return (
      <div style={styles.loginContainer}>
        <h1 style={styles.h1}>login</h1>
        { errorContent }
        { this.props.isLoggingIn ? <div style={styles.auth}>authenticating</div> :
          <form onSubmit={this.handleSubmit.bind(this)} style={styles.form}>
            <input key="username" type="string" value={username} onChange={this.inputChanged.bind(this, 'username')} style={inputStyles} placeholder="Username"/>
            <input key="password" type="password" value={password} onChange={this.inputChanged.bind(this, 'password')} style={inputStyles} placeholder="Password"/>
            <button key="signin" style={styles.button} type="submit">sign in</button>
          </form> }
      </div>
    );
  }
};

var styles = {
  loginContainer: {
    backgroundColor: '#C7C7C7',
    color: '#3B3B35',

    minHeight: '200px',
    width: '400px',
    padding: '10px 20px 10px 20px',
    
    borderTop: '8px solid #8A2B8D',
    borderBottom: '8px solid #8A2B8D',

    marginTop: '40px',
    marginRight: 'auto',
    marginLeft: 'auto',
    marginBottom: '10px',

    '@media (max-width: 768px)': {
      height: '100%',
      width: '90%',
      marginTop: '0px',
      marginBottom: '0px',
      borderBottom: '0'
    }
  },
  h1: {
    color: '#8A2B8D',
    marginBottom: '20px'
  },
  form: {
    textAlign: 'center'
  },
  input: {
    width: '100%',
    marginBottom: '8px',
    padding: '10px 10px',
    outline: '0',
    border: '1px solid #C6C6C6',
    borderRadius: '10px',
    ':focus': {
      border: '1px solid #F727FF',
      boxShadow: '0 0 10px #F727FF'
    }
  },
  inputError: {
    border: '1px solid #FF6771',
    boxShadow: '0 0 6px #FF6771'
  },
  errorMessage: {
    backgroundColor: '#FF6771',
    padding: '20px',
    margin: '20 0 20 0',
    border: '1px solid #CC1521',
    color: 'white'
  },
  button: {
    padding: '15px 60px',
    fontSize: '20px',
    borderRadius: '10px',
    border: '1px solid #8A2B8D',
    width: '100%',
    backgroundColor: '#B152C0',
    color: 'white',
    marginTop: '20px',
    ':hover': {
      backgroundColor: 'white',
      color: '#3B3B35'
    }
  },
  auth: {
    textAlign: 'center',
    marginTop: '60px',
    color: '#8A2B8D'
  }
};

export default connect(state => state.auth)(Login);