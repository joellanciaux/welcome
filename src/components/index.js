import React from 'react';
import Login from './login/login';
import Welcome from './welcome/welcome';
import App from './app';

export default {
  App: App,
  Login: Login,
  Welcome: Welcome
};