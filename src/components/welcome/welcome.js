import React from 'react';
import Radium from 'radium'
import { connect } from 'react-redux';
import { logout } from '../../actions/actions';

@Radium
class Welcome extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let { isLoggedIn } = this.props;

    if (!isLoggedIn) {
      this.props.dispatch(logout(this.context.router));
    }
  }

  logout() {
    // Log the user out
    this.props.dispatch(logout(this.context.router));
  }

  render() {
    return (
      <div style={styles.container}>
        <h1 style={styles.header}>Welcome!</h1>
        <button key="logout" type="button" onClick={this.logout.bind(this)} style={styles.logout}>(logout)</button>
      </div>
    );
  }
};

var styles = {
  container: {
    'textAlign': 'center'
  },
  header: {
    fontSize: '100px',
    color: '#F727FF',
    textShadow: '0 10px 1px #7D3A7F'
  },
  logout: {
    position: 'absolute',
    bottom: '0',
    right: '0',
    margin: '0 20px 20px 0',
    border: '0',
    backgroundColor: 'transparent',
    fontWeight: 'bold',
    fontSize: '20px',
    color: '#CC0B18',
    ':focus' : {
      outline: '0'
    }
  }
};

export default connect(state => {
  return { isLoggedIn: state.auth.isLoggedIn }
})(Welcome);