import React from 'react';
import Radium from 'radium';

@Radium
class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div style={styles.containerStyle}>
        {this.props.children}
      </div>
    );
  }
};

var styles = {
  containerStyle: {
    backgroundColor: '#2E2E2E',
    width: '100%',
    height: '100%',
    overflow: 'auto',
    fontFamily: 'Helvetica Neue, Helvetica, Arial, sans-serif'
  }
};

export default App;