import React from 'react';
import { App, Login, Welcome } from './components/';
import { Router, Route, DefaultRoute } from 'react-router';
import { history } from 'react-router/lib/BrowserHistory';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';
import * as reducers from './reducers/';

const combinedReducer = combineReducers({
  ...reducers
});

// Use the thunk middleware to allow for multiple dispatches in a single action.
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

// Create the store
const store = createStoreWithMiddleware(combinedReducer);

React.render((
  <Provider store={store}>{() => 
      <Router history={history}>
        <Route component={App}>
          <Route name="login" path="/" component={Login} />
          <Route name="welcome" path="/welcome" component={Welcome}/>
        </Route>
      </Router>
    }
  </Provider>
), document.body);